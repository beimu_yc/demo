package com.chen.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.model.system.SysRole;
import com.chen.model.vo.AssginRoleVo;
import com.chen.model.vo.SysRoleQueryVo;

import java.util.Map;

/**
 * @author Y·C
 * @version 1.0.0
 * @ClassName SysRoleService.java
 * @Description TODO
 * @createTime 2023年04月03日 15:28:00
 */

public interface SysRoleService extends IService<SysRole> {

    //条件分页查询
    IPage<SysRole> selectPage(Page<SysRole> pageParam, SysRoleQueryVo sysRoleQueryVo);

    //获取用户的角色数据
    Map<String, Object> getRolesByUserId(Integer userId);

    //用户分配角色
    void doAssign(AssginRoleVo assginRoleVo);

    //更改角色状态
    void updateStatus(Integer id, Integer status);
}

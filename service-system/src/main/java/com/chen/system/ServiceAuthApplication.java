package com.chen.system;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Y·C
 * @version 1.0.0
 * @ClassName ServiceAuthApplication.java
 * @Description TODO
 * @createTime 2023年04月03日 15:10:00
 */
@SpringBootApplication
@MapperScan("com.chen.system.mapper")
public class ServiceAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceAuthApplication.class);
    }

}


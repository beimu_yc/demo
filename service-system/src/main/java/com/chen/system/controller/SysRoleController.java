package com.chen.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.result.Result;
import com.chen.model.system.SysRole;
import com.chen.model.vo.AssginRoleVo;
import com.chen.model.vo.SysRoleQueryVo;
import com.chen.system.annotation.Log;
import com.chen.system.enums.BusinessType;
import com.chen.system.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author Y·C
 * @version 1.0.0
 * @ClassName SysRoleController.java
 * @Description TODO
 * @createTime 2023年04月03日 15:40:00
 */

@Api(tags = "角色管理")
@RestController
@RequestMapping("/admin/system/sysRole")
public class SysRoleController {

    @Resource
    private SysRoleService sysRoleService;


    @ApiOperation("获取用户角色数据")
    @GetMapping("toAssign/{userId}")
    public Result toAssign(@PathVariable Integer userId){
        Map<String,Object> roleMap = sysRoleService.getRolesByUserId(userId);
        return Result.ok(roleMap);
    }

    @ApiOperation("用户分配角色")
    @PostMapping("doAssign")
    public Result doAssign(@RequestBody AssginRoleVo assginRoleVo){

        sysRoleService.doAssign(assginRoleVo);

        return Result.ok();
    }


    /**
     * 批量删除
     * 多个id值[1,2,3]
     * json数组格式---java的list集合
     * */
    @Log(title = "角色管理",businessType = BusinessType.DELETE)
    @PreAuthorize("hasAuthority('bnt.sysRole.remove')")
    @ApiOperation("批量删除")
    @PostMapping("batchRemove")
    public Result batchRemove(@RequestBody List<Long> ids){

        if (sysRoleService.removeByIds(ids)) {
            return Result.ok();
        }else {
            return Result.fail();
        }

    }

    @Log(title = "角色管理",businessType = BusinessType.UPDATE)
    @PreAuthorize("hasAuthority('bnt.sysRole.update')")
    @ApiOperation("修改")
    @PostMapping("update")
    public Result updateRole(@RequestBody SysRole sysRole){

        if (sysRoleService.updateById(sysRole)) {
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    @Log(title = "角色管理",businessType = BusinessType.UPDATE)
    @PreAuthorize("hasAuthority('bnt.sysRole.list')")
    @ApiOperation("根据id查询")
    @PostMapping("findRoleById/{id}")
    public Result findRoleById(@PathVariable Integer id){

        SysRole sysRole = sysRoleService.getById(id);
        return Result.ok(sysRole);
    }

    /**
     *  @RequestBody 不能使用get提交方式
     *传递的是json格式数据，把json格式数据封装到对象里面
     **/
    @Log(title = "角色管理",businessType = BusinessType.INSERT)
    @PreAuthorize("hasAuthority('bnt.sysRole.add')")
    @ApiOperation("添加角色")
    @PostMapping("save")
    public Result saveRole(@RequestBody SysRole sysRole){

        if (sysRoleService.save(sysRole)) {
            return Result.ok();
        }else {
            return Result.fail();
        }

    }

    @PreAuthorize("hasAuthority('bnt.sysRole.list')")
    @ApiOperation("条件分页查询")
    @GetMapping("{page}/{limit}")
    public Result findPageQueryRole(@PathVariable Long page,
                                    @PathVariable Long limit,
                                    SysRoleQueryVo sysRoleQueryVo){
        //创建page对象
        Page<SysRole> pageParam = new Page<>(page,limit);
        //调用service方法进行实现
        IPage<SysRole> pageModel = sysRoleService.selectPage(pageParam,sysRoleQueryVo);

        //返回
        return Result.ok(pageModel);
    }


    @PreAuthorize("hasAuthority('bnt.sysRole.remove')")
    @ApiOperation(value = "根据id逻辑进行删除")
    //2 逻辑删除接口
    @DeleteMapping("remove/{id}")
    public Result removeRole(@PathVariable Integer id){

        if (sysRoleService.removeById(id)) {
            return Result.ok();
        }else {
            return Result.fail();
        }
    }


    @ApiOperation(value = "获取全部角色列表")
    //查询所有记录
    @GetMapping("findAll")
    public Result findAllRole(){

        List<SysRole> list = sysRoleService.list();

        return Result.ok(list);
    }

    @ApiOperation("更改用户状态")
    @GetMapping("updateStatus/{id}/{status}")
    public Result updateStatus(@PathVariable Integer id,
                               @PathVariable Integer status) {
        sysRoleService.updateStatus(id,status);
        return Result.ok();
    }

}

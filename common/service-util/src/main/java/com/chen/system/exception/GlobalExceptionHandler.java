package com.chen.system.exception;

import com.chen.common.result.Result;
import com.chen.common.result.ResultCodeEnum;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice //通过AOP实现添加异常处理
public class GlobalExceptionHandler {

    //1 全局异常
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e) {
        System.out.println("全局....");
        e.printStackTrace();
        return Result.fail().message("执行了全局异常处理");
    }

    //2 特定异常处理
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody
    public Result error(ArithmeticException e) {
        System.out.println("特定......");
        e.printStackTrace();
        return Result.fail().message("执行了特定异常处理");
    }

    //3 自定义异常处理
    @ExceptionHandler(ChenException.class)
    @ResponseBody
    public Result error(ChenException e) {
        e.printStackTrace();
        return Result.fail().code(e.getCode()).message(e.getMsg());
    }

    /**
     * spring security异常
     * @param e
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseBody
    public Result error(AccessDeniedException e) throws AccessDeniedException {

        return Result.fail().code(ResultCodeEnum.PERMISSION.getCode()).message("没有当前功能操作权限");
    }

    /**
     * RedisException异常
     * @param e
     * @return
     */
    @ExceptionHandler(RedisSystemException.class)
    @ResponseBody
    public Result error(RedisSystemException e) throws RedisSystemException {
        return Result.fail().code(ResultCodeEnum.TIME_OUT.getCode()).message("登录超时");
    }


}

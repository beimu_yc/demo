package com.chen.system.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.model.system.SysUser;
import com.chen.model.vo.RouterVo;
import com.chen.model.vo.SysUserQueryVo;
import com.chen.system.mapper.SysUserMapper;
import com.chen.system.service.SysMenuService;
import com.chen.system.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author Y·C
 * @since 2023-04-04
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 根据用户名称获取用户信息（基本信息 和 菜单权限 和 按钮权限数据）
     * */
    @Override
    public Map<String, Object> getUserInfo(String username) {
        //根据username查询用户基本信息
        SysUser sysUser = this.getUserInfoByUserName(username);

        //根据userid查询菜单权限值
        List<RouterVo> routerVoList = sysMenuService.getUserMenuList(sysUser.getId());
        //根据userid查询按钮权限值
        List<String> permsList = sysMenuService.getUserButtonList(sysUser.getId());

        Map<String,Object> result = new HashMap<>();
        result.put("name",username);
        result.put("avatar",sysUser.getHeadUrl());
        result.put("roles",sysUser.getDescription());
        //菜单权限数据
        result.put("routers",routerVoList);
        //按钮权限数据
        result.put("buttons",permsList);


        return result;
    }

    /**
     * 根据用户名称进行查询
     * */
    @Override
    public SysUser getUserInfoByUserName(String username) {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        return baseMapper.selectOne(wrapper);
    }

    /**
     * 用户列表方法
     * */
    @Override
    public IPage<SysUser> selectPage(Page<SysUser> pageParam, SysUserQueryVo sysUserQueryVo) {

        return baseMapper.selectPage(pageParam,sysUserQueryVo);
    }

    /**
     * 更改用户状态
     * */
    @Override
    public void updateStatus(Integer id, Integer status) {
        //根据用户id查询
        SysUser sysUser = baseMapper.selectById(id);

        //设置修改状态
        sysUser.setStatus(status);

        //调用方法修改
        baseMapper.updateById(sysUser);

    }
}

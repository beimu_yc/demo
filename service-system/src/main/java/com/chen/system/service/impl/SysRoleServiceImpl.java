package com.chen.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chen.model.system.SysRole;
import com.chen.model.system.SysUserRole;
import com.chen.model.vo.AssginRoleVo;
import com.chen.model.vo.SysRoleQueryVo;
import com.chen.system.mapper.SysRoleMapper;
import com.chen.system.mapper.SysUserRoleMapper;
import com.chen.system.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Y·C
 * @version 1.0.0
 * @ClassName SysRoleServiceImpl.java
 * @Description TODO
 * @createTime 2023年04月03日 15:30:00
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;


    //条件分页查询
    @Override
    public IPage<SysRole> selectPage(Page<SysRole> pageParam, SysRoleQueryVo sysRoleQueryVo) {

        IPage<SysRole> pageModel = baseMapper.selectPage(pageParam,sysRoleQueryVo);

        return pageModel;
    }


    //获取用户的角色数据
    @Override
    public Map<String, Object> getRolesByUserId(Integer userId) {
        //获取所有角色
        List<SysRole> roles = baseMapper.selectList(null);
        //根据用户id查询，已经分配角色
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId);
        List<SysUserRole> userRolesList = sysUserRoleMapper.selectList(wrapper);
        //从userRoles集合获取所有角色id
        List<Integer> userRoleIds = new ArrayList<>();
        for (SysUserRole userRole : userRolesList) {
            Integer roleId = userRole.getRoleId();
            userRoleIds.add(roleId);
        }
        //封装到map集合
        Map<String,Object> returnMap = new HashMap<>();
        returnMap.put("allRoles",roles);
        returnMap.put("userRoleIds",userRoleIds);

        return returnMap;
    }

    //用户分配角色
    @Override
    public void doAssign(AssginRoleVo assginRoleVo) {
        //根据用户id删除之前分配角色
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",assginRoleVo.getUserId());
        sysUserRoleMapper.delete(wrapper);

        //获取所有角色id，添加角色用户关系表
        //角色id列表
        List<Integer> roleIdList = assginRoleVo.getRoleIdList();
        for (Integer roleId : roleIdList) {
            SysUserRole userRole = new SysUserRole();
            userRole.setUserId(assginRoleVo.getUserId());
            userRole.setRoleId(roleId);
            sysUserRoleMapper.insert(userRole);
        }
    }

    //更改用户状态
    @Override
    public void updateStatus(Integer id, Integer status) {
        //根据用户id查询
        SysRole sysRole = baseMapper.selectById(id);
        //设置修改状态
        sysRole.setStatus(status);
        //调用方法修改
        baseMapper.updateById(sysRole);
    }
}

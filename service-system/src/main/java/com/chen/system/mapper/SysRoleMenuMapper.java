package com.chen.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.model.system.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Y·C
 * @version 1.0.0
 * @ClassName SysRoleMenuMapper.java
 * @Description TODO
 * @createTime 2023年04月05日 11:27:00
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}

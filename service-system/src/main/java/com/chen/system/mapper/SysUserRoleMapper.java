package com.chen.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.model.system.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Y·C
 * @version 1.0.0
 * @ClassName SysUserRoleMapper.java
 * @Description TODO
 * @createTime 2023年04月04日 17:56:00
 */

@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}

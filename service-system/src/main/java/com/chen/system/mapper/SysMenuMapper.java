package com.chen.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.model.system.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author Y·C
 * @since 2023-04-04
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    //根据userid查询
    List<SysMenu> findMenuListUserId(@Param("userId") Integer userId);
}

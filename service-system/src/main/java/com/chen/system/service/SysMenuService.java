package com.chen.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.model.system.SysMenu;
import com.chen.model.vo.AssginMenuVo;
import com.chen.model.vo.RouterVo;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author Y·C
 * @since 2023-04-04
 */
public interface SysMenuService extends IService<SysMenu> {

    //菜单列表(树形)
    List<SysMenu> findNodes();

    //删除菜单
    void removeMenuById(Integer id);

    //根据角色分配菜单
    List<SysMenu> findMenuByRoleId(Integer roleId);

    //给角色分配菜单权限
    void doAssign(AssginMenuVo assginMenuVo);

    //根据userid查询菜单权限值
    List<RouterVo> getUserMenuList(Integer userId);

    //根据userid查询按钮权限值
    List<String> getUserButtonList(Integer userId);

    //更改用户状态
    void updateStatus(String id, Integer status);
}

package com.chen.system.controller;


import com.chen.common.result.Result;
import com.chen.model.system.SysMenu;
import com.chen.model.vo.AssginMenuVo;
import com.chen.system.annotation.Log;
import com.chen.system.enums.BusinessType;
import com.chen.system.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author Y·C
 * @since 2023-04-04
 */
@Api(tags = "菜单管理")
@RestController
@RequestMapping("/admin/system/sysMenu")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;


    @ApiOperation("给用户分配菜单权限")
    @PostMapping("/doAssign")
    public Result doAssign(@RequestBody AssginMenuVo assginMenuVo){

        sysMenuService.doAssign(assginMenuVo);

        return Result.ok();
    }

    @ApiOperation("根据角色获取菜单")
    @GetMapping("/toAssign/{roleId}")
    public Result toAssign(@PathVariable Integer roleId){

        List<SysMenu> list = sysMenuService.findMenuByRoleId(roleId);

        return Result.ok(list);
    }

    //菜单列表（树形）
    @ApiOperation("菜单列表")
    @GetMapping("findNodes")
    public Result findNodes(){

        List<SysMenu> list = sysMenuService.findNodes();

        return Result.ok(list);
    }

    @Log(title = "菜单管理",businessType = BusinessType.INSERT)
    @ApiOperation("添加菜单")
    @PostMapping("save")
    public Result save(@RequestBody SysMenu sysMenu){

        if (sysMenuService.save(sysMenu)) {
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    @ApiOperation("根据id查询菜单")
    @PostMapping("findNode/{id}")
    public Result findNode(@PathVariable Integer id){
        SysMenu sysMenu = sysMenuService.getById(id);
        return Result.ok(sysMenu);
    }

    @Log(title = "修改菜单",businessType = BusinessType.DELETE)
    @ApiOperation("修改菜单")
    @PostMapping("update")
    public Result update(@RequestBody SysMenu sysMenu){
        if (sysMenuService.updateById(sysMenu)) {
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    @Log(title = "菜单管理",businessType = BusinessType.DELETE)
    @ApiOperation("删除菜单")
    @PostMapping("remove/{id}")
    public Result removeById(@PathVariable Integer id){
       sysMenuService.removeMenuById(id);
       return Result.ok();
    }

    @ApiOperation("更改用户状态")
    @GetMapping("updateStatus/{id}/{status}")
    public Result updateStatus(@PathVariable String id,
                               @PathVariable Integer status) {
        sysMenuService.updateStatus(id,status);
        return Result.ok();
    }

}


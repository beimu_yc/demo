package com.chen.model.system;

import com.chen.model.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
//实体类对应的表的名称

@TableName("sys_role")
public class SysRole extends BaseEntity	 {

	private static final long serialVersionUID = 1L;

	//实体类对应的字段的名称

	@TableField("role_name")
	private String roleName;

	@TableField("role_code")
	private String roleCode;


	@ApiModelProperty(value = "状态(0:禁止,1:正常)")
	@TableField("status")
	private Integer status;

	@TableField("description")
	private String description;

}

